-- display configuration
set lines 1000
set pages 1000
column NAME format a10
column DATABASE_COMPATIBILITY format a23
column TOTAL_MB format a18
column FREE_MB format a18
-- end of configuration
SELECT 
    GROUP_NUMBER, 
    NAME, 
    SECTOR_SIZE, 
    BLOCK_SIZE, 
    ALLOCATION_UNIT_SIZE, 
    STATE, 
    TYPE, 
    (TOTAL_MB || ' MB') as TOTAL_MB,
    (FREE_MB || ' MB') as FREE_MB, 
    OFFLINE_DISKS, 
    DATABASE_COMPATIBILITY
FROM 
	V$ASM_DISKGROUP;