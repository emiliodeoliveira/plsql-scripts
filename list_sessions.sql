set lines 1000
set pages 1000

column pid format a10
column ser# format a10
column username format a10
column os_user format a10
column program format a40


select
       substr(a.spid,1,9) pid,
       substr(b.sid,1,5) sid,
       substr(b.serial#,1,5) ser#,
       --substr(b.machine,1,6) box,
       substr(b.username,1,10) username,
--       b.server,
       substr(b.osuser,1,8) os_user,
       substr(b.program,1,30) program,
       b.status
from v$session b, v$process a
where
b.paddr = a.addr
and type='USER'
order by spid;