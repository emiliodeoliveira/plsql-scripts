-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

-- display configuration
set lines 1000
set pages 1000
column username format a18
column machine format a20
column SPID format a10
column osuser format a20
column program format a42
column logon_Time format a20
-- end of configuration

SELECT s.username, s.osuser, s.sid, s.serial#, p.spid, s.status, s.machine, s.program, TO_CHAR(s.logon_Time,'DD-MON-YYYY HH24:MI:SS') AS logon_time
FROM 
	v$session s
inner join 
	v$process p
on 
	s.paddr = p.addr
WHERE 
	s.status = 'ACTIVE'
	and s.type <> 'BACKGROUND';

	-- global dynamic performance view

SELECT s.username, s.osuser, s.sid, s.serial#, p.spid, s.status, s.machine, s.program, TO_CHAR(s.logon_Time,'DD-MON-YYYY HH24:MI:SS') AS logon_time
FROM 
	gv$session s
inner join 
	gv$process p
on 
	s.paddr = p.addr
WHERE 
	s.status = 'ACTIVE'
	and s.type <> 'BACKGROUND';