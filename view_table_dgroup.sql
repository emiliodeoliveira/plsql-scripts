-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

SELECT * 
FROM 
    dba_tablespace_usage_metrics 
order by 
    used_percent desc;

SELECT 
    name, 
    free_mb, 
    total_mb, 
    free_mb/total_mb*100 as percentage 
FROM 
    v$asm_diskgroup;

    
select 
    dg.name, 
    d.name, 
    d.path, 
    d.total_mb, 
    d.free_mb
  from 
    v$asm_diskgroup dg, 
    v$asm_disk d
 where 
    d.group_number = dg.group_number
 order by 
    free_mb, 1, 2;