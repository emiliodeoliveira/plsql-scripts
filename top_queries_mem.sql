-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

set lines 1000
set pages 1000
COLUMN SQL_TEXT                      FORMAT a60

select 
	SQL_ID,
    PROGRAM_ID,
	SQL_TEXT,
	CPU_TIME,
	ELAPSED_TIME,    
    users_executing,
    PARSING_USER_ID		,
    RUNTIME_MEM
from
	gv$sqlarea
where 
    users_executing > 0
order by
    RUNTIME_MEM desc;