-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS';