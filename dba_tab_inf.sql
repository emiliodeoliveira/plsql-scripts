-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

-- display configuration
set lines 1000
set pages 1000
column FILE_NAME format a48
column ID format a2
column BYTES format a8
column MAXBYTES format a11
column USER_BYTES format a10
column T_STATUS format a8
-- end of configuration
SELECT 
    FILE_NAME, 
    to_char(FILE_ID) as ID, 
    (round(BYTES/1024/1024,2) || ' MB') as BYTES, 
    BLOCKS, 
    STATUS, 
    RELATIVE_FNO, 
    AUTOEXTENSIBLE,
    (round(MAXBYTES/1024/1024,2) || ' MB') as MAXBYTES, 
    MAXBLOCKS, 
    INCREMENT_BY, 
    (round(USER_BYTES/1024/1024,2) || ' MB') as USER_BYTES, 
    USER_BLOCKS, 
    ONLINE_STATUS as T_STATUS
FROM 
	DBA_DATA_FILES;