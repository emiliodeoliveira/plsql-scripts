
set lines 1000
set pages 1000
column a.bytes format a48

select sum(a.bytes) as undo_size 
from v$datafile a, v$tablespace b, dba_tablespaces c 
where c.contents = 'UNDO' 
and c.status = 'ONLINE' 
and b.name = c.tablespace_name 
and a.ts# = b.ts#;
