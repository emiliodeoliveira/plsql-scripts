select object_name as table_name
from all_objects t
where object_type = 'TABLE'
and owner = '&SCHEMA_NAME'
order by object_name