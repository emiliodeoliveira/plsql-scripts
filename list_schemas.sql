set lines 1000
set pages 1000
column owner format a30
column bytes format a20
select
    owner, (sum(bytes)/1024/1024 || ' MB') as BYTES
from 
   dba_segments
where 
   owner not in ('SYSTEM', 'XDB', 'SYS', 'TSMSYS', 'MDSYS', 'EXFSYS', 'WMSYS', 'ORDSYS', 'OUTLN', 'DBSNMP')
   group by owner;