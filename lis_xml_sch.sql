-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

set lines 1000
set pages 1000
column owner format a20
column local format a5
column schema_url format a80

select 
    x.OWNER, 
    schema_url, 
    int_objname, 
    status 
from 
    dba_xml_schemas x, dba_objects o 
where
    owner='&owner' 
    and int_objname = object_name and status = 'VALID';
order by 1,2;