select 
	total_mb/1024 "Total (GB)", 
	round(free_mb/1024) "Free (GB)", 
	round(((total_mb/1024)-(free_mb/1024))) "Used (GB)", 
	round((((total_mb/1024)-(free_mb/1024))*100/(total_mb/1024)))||'%' "Percent Usage (%)" 
from 
	v$asm_diskgroup 
	where name='DATA_1'; 