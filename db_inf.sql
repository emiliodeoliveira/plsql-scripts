-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

-- display configuration
set lines 1000
set pages 1000
column INSTANCE_NAME format a18
column DB_UNIQUE_NAME format a18
column CONTROLFILE_CREATED format a20
column OPEN_MODE format a10
column HOST_NAME format a30
-- end of configuration

SELECT * FROM v$version;

SELECT 
	INSTANCE_NUMBER, 
	INSTANCE_NAME, 
	HOST_NAME,
	STARTUP_TIME, 
	STATUS, 
	DATABASE_STATUS
FROM 
	gv$INSTANCE;
    
SELECT 
	NAME, 
	DB_UNIQUE_NAME, 
	DBID, 
	LOG_MODE, 
	FLASHBACK_ON, 
	CONTROLFILE_CREATED,
	OPEN_MODE	
FROM 
	gv$DATABASE;