set long 20000 longchunksize 20000 pagesize 0 linesize 1000 feedback off verify off trimspool on set head off
set verify off

column ddl format a1000

accept u prompt 'Username: '

spool c:/temp/ddl_create_user.sql

--select dbms_metadata.get_ddl( 'USER', '&u' ) from dual;
--select dbms_metadata.get_granted_ddl( 'SYSTEM_GRANT', '&u' ) from dual;
--select dbms_metadata.get_granted_ddl( 'ROLE_GRANT', '&u' ) from dual;
--select dbms_metadata.get_granted_ddl( 'OBJECT_GRANT', '&u' ) from dual;

select 'create user '||username||' identified by values '||REGEXP_SUBSTR(DBMS_METADATA.get_ddl ('USER',USERNAME), '''[^'']+''')||' default tablespace '||default_tablespace||' temporary tablespace '||temporary_tablespace||' profile '||profile||';'
from dba_users where username = upper('&u');

select distinct 'create role '||granted_role||';' from dba_role_privs where grantee like upper('&u');

select 'grant '||privilege||' on '||owner||'.'||table_name||' to '||grantee||decode(grantable,'YES',' with grant option',null)||';' from dba_tab_privs where grantee like upper('&u')
union all
select 'grant '||privilege||' to '||grantee||decode(admin_option,'YES',' with admin option',null)||';' from dba_sys_privs where grantee like upper('&u')
union all
select 'grant '||granted_role||' to '||grantee||decode(admin_option,'YES',' with admin option',null)||';' from dba_role_privs where grantee like upper('&u');

prompt alter user &u default role all;;

spool off

prompt 
prompt 
prompt === Generated script: c:/temp/ddl_create_user.sql

set feedback on
set pagesize 100
set head on
set verify on
