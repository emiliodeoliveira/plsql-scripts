set lines 1000
select 
   table_name, owner, (round((blocks*8),2))/1024 "Size (mb)" , 
   (round((num_rows*avg_row_len/1024),2))/1024 "Actual Data (mb)",
   (round((blocks*8),2) - round((num_rows*avg_row_len/1024),2))/1024 "Wasted Space (mb)",
   (((round((blocks*8),2) - round((num_rows*avg_row_len/1024),2))/1024)*100)/((round((blocks*8),2))/1024) "Wasted in %" 
from 
   dba_tables
where 
   (round((blocks*8),2) > round((num_rows*avg_row_len/1024),2))
order by 5 desc;