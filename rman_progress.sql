column "time started" justify center
column opname for a30
col client_info for a22
col event for a30
col "Time Started" for a30
set lines 1000
set pages 1000
set trims on

prompt 
prompt #####################################
prompt RMAN PROGESS
prompt #####################################
prompt 

select opname,trunc((totalwork*8192)/1024/1024) "Total work(MB)",
trunc((sofar*8192)/1024/1024) "Work Done(MB)", 
to_char(start_time, 'MM-DD-YYYY HH24:MI:SSAM') "Time Started", 
trunc(time_remaining/60) "Time Remaining(min)",
round(elapsed_seconds/60) "Time Elasped(min)", 
round(sofar/totalwork*100,2) "% Complete"
from v$session_longops
where opname like 'RMAN%'
and    opname NOT LIKE '%aggregate%'
and totalwork != 0
and sofar <> totalwork
order by start_time, totalwork desc
/

prompt 
prompt #####################################
prompt RMAN WAITS
prompt #####################################
prompt 

select 
   sid, 
   spid, 
   client_info, 
   event, 
   seconds_in_wait, 
   p1, p2, p3
 from 
   v$process p, 
   v$session s
 where 
   p.addr = s.paddr
 and 
   client_info like 'rman channel=%'
/