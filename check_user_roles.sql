set lines 1000
set pages 1000

select 
    granted_role,
    admin_option,
    default_role 
from 
    dba_role_privs 
where 
    grantee='&USER';