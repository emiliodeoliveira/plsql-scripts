-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

-- display configuration
set lines 1000
set pages 1000
column directory_name format a30
column directory_path format a70
-- end of configuration 
select 
    directory_name, 
    directory_path 
from 
    dba_directories 
order by 
    directory_name;

