set serveroutput on size 1000000
col owner for a20
col job_name for a20
col operation for a20
col state for a20
col message for a35

prompt 
prompt ### CURRENT DATAPUMP JOBS RUNNING
select owner_name owner,job_name,operation,state from dba_datapump_jobs order by 3,1;

accept p_owner prompt 'Enter Datapump Owner: '
accept p_job prompt 'Enter Datapump Job Name: '
prompt 

prompt ### DATAPUMP JOB STATUS
DECLARE
  ind NUMBER;              -- Loop index
  h1 NUMBER;               -- Data Pump job handle
  percent_done NUMBER;     -- Percentage of job complete
  job_state VARCHAR2(30);  -- To keep track of job state
  js ku$_JobStatus;        -- The job status from get_status
  ws ku$_WorkerStatusList; -- Worker status
  sts ku$_Status;          -- The status object returned by get_status
BEGIN
h1 := DBMS_DATAPUMP.attach(upper('&p_job'), upper('&p_owner')); -- job name and owner
dbms_datapump.get_status(h1,
           dbms_datapump.ku$_status_job_error +
           dbms_datapump.ku$_status_job_status +
           dbms_datapump.ku$_status_wip, 0, job_state, sts);
js := sts.job_status;
ws := js.worker_status_list;
      dbms_output.put_line('*** Job percent done: ' || to_char(js.percent_done) || '%');
      dbms_output.put_line('*** Restarts: '||js.restart_count);
ind := ws.first;
  while ind is not null loop
    dbms_output.put_line('*** Rows completed: '||ws(ind).completed_rows);
    ind := ws.next(ind);
  end loop;
DBMS_DATAPUMP.detach(h1);
end;
/

prompt ### ADDITIONAL DETAILS ABOUT DATAPUMP JOB
select s.sid,s.username,l.start_time,l.elapsed_seconds,l.TIME_REMAINING,
         (sofar/totalwork)*100 "%done",l.message
  from v$session s, v$session_longops l
  where s.sid = l.sid and
        s.status = 'ACTIVE' and
        (sofar/totalwork) < 1 and
       totalwork > 0 and
	   l.target_desc in ('EXPORT','IMPORT') and
	   l.opname = upper('&p_job');
	   
prompt ### How to kill a datapump job
prompt DECLARE
prompt    h1 NUMBER;
prompt BEGIN
prompt    h1 := DBMS_DATAPUMP.ATTACH('p_job','p_owner');;
prompt    DBMS_DATAPUMP.STOP_JOB (h1,1,0);;
prompt END;
prompt /
