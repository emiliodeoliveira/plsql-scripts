-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

col message for a35

select s.inst_id,s.sid,s.username,l.start_time,l.elapsed_seconds,l.TIME_REMAINING,
         (sofar/totalwork)*100 "%done",l.message
  from gv$session s, gv$session_longops l
  where s.inst_id = l.inst_id and
        s.sid = l.sid and
        s.status = 'ACTIVE' and
        (sofar/totalwork) < 1 and
       totalwork > 0
/