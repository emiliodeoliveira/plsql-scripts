-- display configuration
set lines 1000
set pages 1000
column OUTPUT format a40
-- end of configuration 
SELECT 
    SID, 
    RECID,
    STAMP,
    SESSION_RECID,
    SESSION_STAMP,
    OUTPUT,  
    RMAN_STATUS_RECID, 
    RMAN_STATUS_STAMP, 
    SESSION_KEY
FROM 
	
    GV$RMAN_OUTPUT;