-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

set lines 1000
set pages 1000
COLUMN SQL_TEXT FORMAT a60
COLUMN username FORMAT a15
COLUMN CPU_TIME      HEADING "CPU_TIME (s)"
select
    e.username,
    e.status,
    a.uga_memory,
    b.pga_memory,
    u.users_executing,    
    TO_CHAR(ROUND(u.RUNTIME_MEM/1024),99999999) || ' KB' RUNTIME_MEM, 
    u.CPU_TIME/1000/1000  CPU_TIME,
    u.SQL_TEXT
from
 (select y.SID, 
  TO_CHAR(ROUND(y.value/1024/1024),99999999) || ' MB' UGA_MEMORY 
  from 
     gv$sesstat y, 
     v$statname z 
   where 
     y.STATISTIC# = z.STATISTIC# 
   and 
     NAME = 'session uga memory') a,
 (select 
     y.SID, 
     TO_CHAR(ROUND(y.value/1024/1024),99999999) || ' MB' PGA_MEMORY   
  from 
     v$sesstat y, v$statname z 
  where 
     y.STATISTIC# = z.STATISTIC#   
  and 
     NAME = 'session pga memory') b,
v$session e
inner join 
    gv$sqlarea u 
on 
    u.SQL_ID=e.SQL_ID 
where
   e.sid=a.sid 
and 
   e.sid=b.sid
order by
   e.status,
   b.pga_memory desc; 