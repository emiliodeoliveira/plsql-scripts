-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

----------------------------------------
prompt AWR_RPT.SQL - AWR REPORT
-- This script generate AWR report in a local folder.
set verify off;
--  v 1.0 Daniel Santos (daniel_e_santos@dell.com) 
----------------------------------------

set lines 500
set pages 5000
set echo off
Set serveroutput on size 1000000
alter session set nls_date_format='dd-mon-yy hh24:mi:ss';

BREAK ON INSTANCE_NUMBER;

column DAY 			format A12
column SNAPSHOTS 	format A250
 
prompt "List of AWR snapshots, how many days ago ?"  
DEFINE DAY = &DAY
 
BREAK ON DAY;
 
 select TX DAY, LISTAGG(KINDOM,'->') WITHIN GROUP (ORDER BY TX,KINDOM) SNAPSHOTS
 from ( select to_char(BEGIN_INTERVAL_TIME,'dd month') TX, SNAP_ID || '-' || to_char(BEGIN_INTERVAL_TIME,'hh:mi am') KINDOM
		 from DBA_HIST_SNAPSHOT
		 where trunc(cast(BEGIN_INTERVAL_TIME as DATE)) >= trunc(sysdate) - nvl('&day',0)
		   and dbid = (select DBID from v$database)
		   and instance_number = 1
		   and cast(BEGIN_INTERVAL_TIME as DATE) < trunc(cast(BEGIN_INTERVAL_TIME as DATE)) + 1/24*12
		 order by SNAP_ID)
 group by TX
UNION 
 select TX DAY, LISTAGG(KINDOM,'->') WITHIN GROUP (ORDER BY TX,KINDOM) SNAPSHOTS
 from ( select to_char(BEGIN_INTERVAL_TIME,'dd month') TX, SNAP_ID || '-' || to_char(BEGIN_INTERVAL_TIME,'hh:mi am') KINDOM
		 from DBA_HIST_SNAPSHOT
		 where trunc(cast(BEGIN_INTERVAL_TIME as DATE)) >= trunc(sysdate) - nvl('&day',0)
		   and dbid = (select DBID from v$database)
		   and instance_number = 1
		   and cast(BEGIN_INTERVAL_TIME as DATE) >= trunc(cast(BEGIN_INTERVAL_TIME as DATE)) + 1/24*12
		 order by SNAP_ID)
 group by TX
order by DAY; 

UNDEFINE DAY
clear breaks
clear columns
COLUMN coluna NEW_VAL X
select dbid coluna from v$database;
prompt "Please define the FILENAME, suggestion >>"
COLUMN nome NEW_VAL Y
select 'c:\temp\' || NAME || '_' || to_char(sysdate,'hh24miss') || '.html' nome from v$database;
DEFINE TARGET_FILE = &TARGET_FILE;
spool &TARGET_FILE;
SELECT output FROM TABLE (dbms_workload_repository.awr_report_html (&X,&INSTANCE_NUMBER,&SNAP_BEGIN,&SNAP_END));
spool off;
UNDEFINE INSTANCE_NUMBER
UNDEFINE SNAP_BEGIN
UNDEFINE SNAP_END
UNDEFINE TARGET_FILE



