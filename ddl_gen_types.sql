-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

SET LONG 20000 LONGCHUNKSIZE 20000 PAGESIZE 0 LINESIZE 1000 FEEDBACK OFF VERIFY OFF TRIMSPOOL ON

BEGIN
   DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'SQLTERMINATOR', true);
   DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'PRETTY', true);
   DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'SEGMENT_ATTRIBUTES', true);
   DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'STORAGE', false); 

END;
/

select 
    dbms_metadata.get_ddl(object_type,object_name,owner) 
from 
    dba_objects 
where 
    owner='$owner_name' and object_type='TYPE';


SET PAGESIZE 14 LINESIZE 100 FEEDBACK ON VERIFY ON