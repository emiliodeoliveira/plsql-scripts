-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

-- display configuration
set lines 1000
set pages 1000
column START_TIME format a20
column END_TIME format a20
column INPUT_BYTES format a15
column OUTPUT_BYTES format a15

ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS';
-- end of configuration 
SELECT 
    SESSION_KEY, 
    INPUT_TYPE, 
    START_TIME, 
    END_TIME, 
    (round(INPUT_BYTES/1024/1024,2) || ' MB') as INPUT_BYTES, 
    (round(OUTPUT_BYTES/1024/1024,2) || ' MB') as OUTPUT_BYTES, 
    OUTPUT_DEVICE_TYPE,  
    STATUS, 
    OPTIMIZED, 
    COMPRESSION_RATIO
FROM 
	V$RMAN_BACKUP_JOB_DETAILS;