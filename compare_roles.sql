set lines 1000 pages 999
COL TYPE FORMAT a12
COL GRANTEE FORMAT a30
COL TABLE_NAME FORMAT a20
COL GRANTED_ROLE FORMAT a20
COL PRIVILEGE a15
COL OWNER FORMAT a15
COL status FORMAT a6

select * from (
	select 
		'ROLE_PRIVS' TYPE,
		GRANTED_ROLE,
		GRANTEE,
		'' TABLE_NAME,
		'' PRIVILEGE,
		'' OWNER
	from
		dba_role_privs@ALAEP_LINK db2
	where (db2.GRANTEE,db2.GRANTED_ROLE) not in (
		select 
			GRANTEE,
			GRANTED_ROLE 
		from 
			dba_role_privs
		where 
			GRANTEE like 'ALAE%'
		)
	and 
		GRANTEE like 'ALAE%'
	union all
	select
		'TABLE_PRIVS' TYPE,
		'' GRANTED_ROLE,
		GRANTEE,
		TABLE_NAME,
		PRIVILEGE,
		OWNER
	from
		dba_tab_privs@ALAEP_LINK db2
		where (db2.GRANTEE,db2.TABLE_NAME,db2.PRIVILEGE,db2.OWNER) not in (
		select 
			GRANTEE,
			TABLE_NAME,
			PRIVILEGE,
			OWNER
		from 
			dba_tab_privs
		where 
			GRANTEE like 'ALAE%'
		)
	and 
		GRANTEE like 'ALAE%'
	union all
	select
		'SYS_PRIVS' TYPE,
		'' GRANTED_ROLE,
		GRANTEE,
		'' TABLE_NAME,
		PRIVILEGE,
		'' OWNER
	from
		dba_sys_privs@ALAEP_LINK db2
	where (db2.GRANTEE,db2.PRIVILEGE) not in (
		select 
			GRANTEE,
			PRIVILEGE			
		from 
			dba_sys_privs
		where 
			GRANTEE like 'ALAE%'
		)
	and 
		GRANTEE like 'ALAE%'
	)
order by GRANTED_ROLE;