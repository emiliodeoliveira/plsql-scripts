-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

set lines 1000
set pages 1000
COLUMN SQL_TEXT                         FORMAT a60
COLUMN username                         FORMAT a15
select
    h.SQL_ID,
    h.SQL_TEXT,
    s.PERSISTENT_MEM,
    su.TOTAL_SQL_MEM    
from
    DBA_HIST_SQLTEXT h
inner join 
    V$SQL s 
on 
    h.SQL_ID=s.SQL_ID 
inner join
    DBA_HIST_SQLSTAT y
on  
    s.SQL_ID=y.SQL_ID 
inner join
    DBA_HIST_SQL_SUMMARY su
on  
    su.SNAP_ID=y.SNAP_ID 
where
    rownum < 11   
order by
   su.TOTAL_SQL_MEM desc; 
