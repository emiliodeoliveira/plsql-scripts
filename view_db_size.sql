set lines 2000
set pages 2000
comp sum of totsiz avasiz on report
break on report
col tsname  format         a30  heading 'Tablespace'
col nfrags  format       99990  heading 'Nro|Frags'
col mxfrag  format 999,999,990  heading 'Maior|Frag(Mb)'
col avfrag  format 999,999,990  heading 'Média|Frags(Mb)'
col totsiz  format 9,999,999,990  heading 'Total|(Mb)'
col avasiz  format 999,999,990  heading 'Livre|(Mb)'
col pctfre  format         a04  heading 'Percentual|Livre'
col warn    format          a1  heading 'W'
select
	total.tablespace_name                       tsname,
	count(free.bytes)                           nfrags,
	nvl(max(free.bytes)/1024/1024,0)                 mxfrag,
	nvl(avg(free.bytes)/1024/1024,0)                 avfrag,
	total.bytes/1024/1024                            totsiz,
	nvl(sum(free.bytes)/1024/1024,0)                 avasiz,
    round(nvl(sum(free.bytes),0)/total.bytes*100)||'%'      pctfre,
	decode(greatest(100*nvl(sum(free.bytes),0)/total.bytes,10),
		10,'*', ' ')                         warn
from
	(       select  tablespace_name,
			sum(bytes)        bytes
		from    sys.dba_data_files
		group by tablespace_name) total,
	dba_free_space  free
where
	total.tablespace_name = free.tablespace_name(+)
group by
	total.tablespace_name,
	total.bytes
/
