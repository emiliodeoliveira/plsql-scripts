SET SERVEROUTPUT ON
BEGIN
  DBMS_OUTPUT.put_line('Time 1: ' || TO_CHAR(SYSTIMESTAMP, 'HH24:MI:SS.FF'));

  -- Pause for 1 second.
  DBMS_LOCK.sleep(10);

  DBMS_OUTPUT.put_line('Time 2: ' || TO_CHAR(SYSTIMESTAMP, 'HH24:MI:SS.FF'));

  -- Pause for half a second.
  DBMS_LOCK.sleep(10);

  DBMS_OUTPUT.put_line('Time 3: ' || TO_CHAR(SYSTIMESTAMP, 'HH24:MI:SS.FF'));
END;
/ 