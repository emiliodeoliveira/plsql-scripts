-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

set long 3000000 longchunksize 20000 pagesize 0 linesize 1000 feedback off verify off trimspool on
column ddl format a1000

begin
   dbms_metadata.set_transform_param (dbms_metadata.session_transform, 'SQLTERMINATOR', true);
   dbms_metadata.set_transform_param (dbms_metadata.session_transform, 'PRETTY', true);
end;
/
 
variable v_role VARCHAR2(30);
-- exec :v_role := 'ALAE%';
select 
	dbms_metadata.get_granted_ddl('ROLE_GRANT', username) AS ddl, dbms_metadata.get_granted_ddl('SYSTEM_GRANT', username) AS ddl2, dbms_metadata.get_granted_ddl('OBJECT_GRANT', username) AS ddl3
from 
	dba_users
where 
	username like 'ALAE%'
-- union all
-- select 
-- 	dbms_metadata.get_granted_ddl('ROLE_GRANT', rp.grantee) AS ddl
-- from   
-- 	dba_role_privs rp
-- where  
-- 	rp.grantee like 'ALAE%'
-- union all
-- select 
-- 	dbms_metadata.get_granted_ddl('SYSTEM_GRANT', sp.grantee) AS ddl
-- from   
-- 	dba_sys_privs sp
-- where  
-- 	sp.grantee like 'ALAE%'
-- union all
-- select 
-- 	dbms_metadata.get_granted_ddl('OBJECT_GRANT', tp.grantee) AS ddl
-- from   
-- 	dba_tab_privs tp
-- where  
-- 	tp.grantee like 'ALAE%'
/

set linesize 80 pagesize 14 feedback on verify on