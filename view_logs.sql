-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

select 
   originating_timestamp,
   message_text
from 
   v$diag_alert_ext
where 
   message_level=1 
and
   originating_timestamp > systimestamp - 15 
and
   component_id like 'rdb%';