COLUMN alme     HEADING "Allocated MB" FORMAT 99999D9
COLUMN usme     HEADING "Used MB"      FORMAT 99999D9
COLUMN frme     HEADING "Freeable MB"  FORMAT 99999D9
COLUMN mame     HEADING "Max MB"       FORMAT 99999D9
COLUMN username                        FORMAT a15
COLUMN pid_remote                      FORMAT a15
COLUMN program                         FORMAT a22
COLUMN sid                             FORMAT a5
COLUMN spid                            FORMAT a8
COLUMN logon_time                      FORMAT a20
SET LINESIZE 300
SET PAGES 200

SELECT 
    s.username, 
    SUBSTR(s.sid,1,5) sid, 
    p.spid, 
    logon_time,
    SUBSTR(s.program,1,22) program , 
    s.process pid_remote,
    s.status,
    ROUND(pga_used_mem/1024/1024) usme,
    ROUND(pga_alloc_mem/1024/1024) alme,
    ROUND(pga_freeable_mem/1024/1024) frme,
    ROUND(pga_max_mem/1024/1024) mame
FROM  
    v$session s,
    v$process p
WHERE 
    p.addr=s.paddr 
    and s.status= upper('&status')
ORDER BY 
    pga_max_mem desc,
    logon_time;


COLUMN category      HEADING "Category"
COLUMN allocated     HEADING "Allocated bytes"
COLUMN used          HEADING "Used bytes"
COLUMN max_allocated HEADING "Max allocated bytes"

SELECT 
    pid, 
    category, 
    allocated, 
    used, 
    max_allocated
FROM   
    v$process_memory
WHERE  
    pid = (SELECT pid
              FROM   v$process
              WHERE  addr= (select paddr
                            FROM   v$session
                            WHERE  sid = &sid));