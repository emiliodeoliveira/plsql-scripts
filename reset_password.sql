set long 90000

accept p prompt 'Username: '
accept p2 prompt 'Password (Just enter to keep current password): '

set lines 200
col instructions for a150

select 'create profile temp limit PASSWORD_VERIFY_FUNCTION null PASSWORD_REUSE_TIME unlimited PASSWORD_REUSE_MAX unlimited;'||chr(10)||
       'alter user '||username||' identified by '||decode('&p2','','values '||chr(39)||replace(REGEXP_SUBSTR(DBMS_METADATA.get_ddl ('USER',USERNAME), '''[^'']+'''),chr(39),'')||chr(39),'&p2')||' profile temp;'||chr(10)||
       'alter user '||username||' profile '||profile||';'||chr(10)||
       'drop profile temp;' INSTRUCTIONS
from dba_users, v$instance where username in (upper('&p'));