-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

SET LONG 20000 LONGCHUNKSIZE 20000 PAGESIZE 0 LINESIZE 1000 FEEDBACK OFF VERIFY OFF TRIMSPOOL ON

BEGIN
   DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'SQLTERMINATOR', true);
   DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'PRETTY', true);
END;
/

select dbms_metadata.get_ddl('INDEX', index_name, owner)
from all_indexes
where owner in 'GOAL_DEV85_FS'
and index_name in ('IX_174891435', 'IX_01712365911', 'SYS_PRODUCTCOST_L3_P1');

SET PAGESIZE 14 LINESIZE 100 FEEDBACK ON VERIFY ON