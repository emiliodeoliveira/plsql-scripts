break on c1 skip 2

set lines 1000
set pages 1000

col c1 heading 'owner' format a35
col c2 heading 'name' format a40
col c3 heading 'type' format a10

ttitle 'Invalid|Objects'

select 
   owner       c1, 
   object_type c3, count(*)  
from 
   dba_objects 
where 
   status != 'VALID'
group by
	owner       , 
   	object_type 
order by
   owner,
   object_type;