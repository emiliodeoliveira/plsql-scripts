-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

set lines 1000
set pages 1000

SELECT 
    owner_name, 
    job_name, 
    operation, 
    job_mode, 
    state
FROM 
    dba_datapump_jobs;