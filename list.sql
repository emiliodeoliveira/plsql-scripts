set feedback off
set lines 1000
set trimspool on
col SCRIPT for a30
col PARAMETERS for a40
col DESCRIPTION for a70

prompt 
prompt 
prompt ########################################################################
prompt DBA SCRIPTS
prompt ########################################################################
prompt 

accept kw prompt 'Keyword (just enter to show all): '

select * from (
select 'conn.sql' SCRIPT,' ' PARAMETERS,'Connect on database with user/password' DESCRIPTION, 'conn' Keywords from dual
union all
select 'conn2.sql' SCRIPT,' ' PARAMETERS,'Connect on database with griddba' DESCRIPTION, 'conn,griddba' Keywords from dual
union all
select 'conn3.sql' SCRIPT,' ' PARAMETERS,'Connect on database with ora_readonly' DESCRIPTION, 'conn,ora_readonly' Keywords from dual
union all
select 'conn4.sql' SCRIPT,' ' PARAMETERS,'Connect on database with admfabio_m_fonseca' DESCRIPTION, 'conn,admfabio_m_fonseca' Keywords from dual
union all
select 'awr.sql' SCRIPT,' ' PARAMETERS,'List all AWRs' DESCRIPTION, 'awr,snapshot,list' Keywords from dual
union all
select 'awrconfig.sql' SCRIPT,' ' PARAMETERS,'Show current AWR configuration' DESCRIPTION, 'awr,config' Keywords from dual
union all
select 'awrcreate.sql' SCRIPT,' ' PARAMETERS,'Take manual awr' DESCRIPTION, 'awr,snapshot,create,manual' Keywords from dual
union all
select 'awrdrop.sql' SCRIPT,'begin snap, end snap' PARAMETERS,'Drop specific AWR' DESCRIPTION, 'awr,snapshot,drop' Keywords from dual
union all
select 'awrmodify.sql' SCRIPT,'retention, interval' PARAMETERS,'Modify AWR settings' DESCRIPTION, 'awr,snapshot,modify' Keywords from dual
union all
select 'asminfo.sql' SCRIPT,' ' PARAMETERS,'Show ASM disk groups info' DESCRIPTION, 'asm,diskgroup' Keywords from dual
union all
select 'autoextend.sql' SCRIPT,' ' PARAMETERS,'Show all datafiles with autoextend enabled' DESCRIPTION, 'datafiles,autoextend' Keywords from dual
union all
select 'autoextend_off.sql' SCRIPT,' ' PARAMETERS,'Disable all datafiles with autoextend enabled' DESCRIPTION, 'datafiles,autoextend' Keywords from dual
union all
select 'baselines.sql' SCRIPT,'sql text, sql handle, sql plan' PARAMETERS,'Shows exising baselines' DESCRIPTION, 'baseline,sqlplan,list' Keywords from dual
union all
select 'db_assessment.sql' SCRIPT,' ' PARAMETERS,'Generate a report for a database' DESCRIPTION, 'assessment,db,preprod,sizing' Keywords from dual
union all
select 'enable_trace.sql' SCRIPT,' ' PARAMETERS,'Enable a session trace' DESCRIPTION, 'session,trace,enable' Keywords from dual
union all
select 'disable_trace.sql' SCRIPT,' ' PARAMETERS,'Disable a session trace' DESCRIPTION, 'session,trace,disable' Keywords from dual
union all
select 'tracefile.sql' SCRIPT,' ' PARAMETERS,'Show trace file name' DESCRIPTION, 'session,trace,filename' Keywords from dual
union all
select 'sw.sql' SCRIPT,'instance id, sid, username, OS user, machine' PARAMETERS,'Shows active sessions' DESCRIPTION, 'session,active' Keywords from dual
union all
select 'killuser.sql' SCRIPT,'username, instance id' PARAMETERS,'Kill all sessions of specific user' DESCRIPTION, 'session,kill,user' Keywords from dual
union all
select 'killrman.sql' SCRIPT,'instance id' PARAMETERS,'Kill all rman sessions' DESCRIPTION, 'session,kill,rman' Keywords from dual
union all
select 'killsql.sql' SCRIPT,'sql id, instance id' PARAMETERS,'Kill all sessions of sql id' DESCRIPTION, 'session,kill,sql' Keywords from dual
union all
select 'killsid.sql' SCRIPT,'sid, instance id' PARAMETERS,'Kill all sessions of sid' DESCRIPTION, 'session,kill,sid' Keywords from dual
union all
select 'killprogram.sql' SCRIPT,'program, instance id' PARAMETERS,'Kill all sessions of program' DESCRIPTION, 'session,kill,program' Keywords from dual
union all
select 'killpin.sql' SCRIPT,'program, instance id' PARAMETERS,'Kill all sessions of library cache pin' DESCRIPTION, 'session,kill,pin,wait' Keywords from dual
union all
select 'killmachine.sql' SCRIPT,'machine, instance id' PARAMETERS,'Kill all sessions of machine' DESCRIPTION, 'session,kill,machine' Keywords from dual
union all
select 'st.sql' SCRIPT,'sid, instance id' PARAMETERS,'Shows current statement of given sid' DESCRIPTION, 'session,sid' Keywords from dual
union all
select 'st2.sql' SCRIPT,'sql id' PARAMETERS,'Shows sql statement based on sql id' DESCRIPTION, 'session,sql id' Keywords from dual
union all
select 'sqlbindval.sql' SCRIPT,'sql id, instance id' PARAMETERS,'Shows bind values of sql id' DESCRIPTION, 'session,sql id,bind,values' Keywords from dual
union all
select 'session.sql' SCRIPT,'instance id' PARAMETERS,'Shows all sessions on specific instance' DESCRIPTION, 'session,list' Keywords from dual
union all
select 'reset_gridbda.sql' SCRIPT,' ' PARAMETERS,'Reset griddba password' DESCRIPTION, 'user,griddba,reset,password' Keywords from dual
union all
select 'reset_password.sql' SCRIPT,'username, password' PARAMETERS,'Reset user password' DESCRIPTION, 'user,reset,password' Keywords from dual
union all
select 'invalid_index.sql' SCRIPT,' ' PARAMETERS,'Check invalid indexes' DESCRIPTION, 'index,invalid,partition' Keywords from dual
union all
select 'dir.sql' SCRIPT,' ' PARAMETERS,'List all existing directories' DESCRIPTION, 'directory,list' Keywords from dual
union all
select 'param.sql' SCRIPT,'parameter name' PARAMETERS,'Check if a parameter requires downtime or not' DESCRIPTION, 'parameter' Keywords from dual
union all
select 'obj.sql' SCRIPT,'object name' PARAMETERS,'Show object details' DESCRIPTION, 'object,info,list' Keywords from dual
union all
select 'temp.sql' SCRIPT,' ' PARAMETERS,'Show Temp tablespace size/usage' DESCRIPTION, 'temp,info,list' Keywords from dual
union all
select 'datapump.sql' SCRIPT,' ' PARAMETERS,'Show datapump jobs progress' DESCRIPTION, 'datapump,progress' Keywords from dual
union all
select 'ddl.sql' SCRIPT,'owner, object name and type' PARAMETERS,'DDL of specific object' DESCRIPTION, 'object,ddl' Keywords from dual
union all
select 'processes.sql' SCRIPT,' ' PARAMETERS,'Show current usage of processes' DESCRIPTION, 'processes,monitor' Keywords from dual
union all
select 'ctl_limits.sql' SCRIPT,' ' PARAMETERS,'Show control file limits' DESCRIPTION, 'controlfile,limits,monitor' Keywords from dual
union all
select 'option.sql' SCRIPT,'option name' PARAMETERS,'Check if an option is installed' DESCRIPTION, 'option,installed,monitor' Keywords from dual
union all
select 'user.sql' SCRIPT,'username' PARAMETERS,'Show additional info from user' DESCRIPTION, 'user,info' Keywords from dual
union all
select 'size.sql' SCRIPT,'owner,object name' PARAMETERS,'Show object size' DESCRIPTION, 'object,size,info' Keywords from dual
union all
select 'killidle.sql' SCRIPT,'instance id, username, period' PARAMETERS,'Kill all idle sessions based on period' DESCRIPTION, 'session,kill,idle' Keywords from dual
union all
select 'sqlprofile.sql' SCRIPT,'sql profile, action' PARAMETERS,'Drop/Enable/Disable a sql profile' DESCRIPTION, 'sqlprofile,status,enable,disable,drop' Keywords from dual
union all
select 'sga_pga_usage.sql' SCRIPT,'how many hours' PARAMETERS,'Show SGA and PGA usage per hour' DESCRIPTION, 'sga,pga,usage,report' Keywords from dual
union all
select 'fra_inf.sql' SCRIPT,' ' PARAMETERS,'Show FRA stats' DESCRIPTION, 'fra,recovery' Keywords from dual
union all
select 'restore_point.sql' SCRIPT,' ' PARAMETERS,'Show existing restore points' DESCRIPTION, 'fra,recovery,restore,point' Keywords from dual
union all
select 'progress.sql' SCRIPT,' ' PARAMETERS,'Show current sessions progress' DESCRIPTION, 'progress,session_longops,monitor' Keywords from dual
union all
select 'dg_prim_diag.sql' SCRIPT,' ' PARAMETERS,'Collect data guard settings from primary' DESCRIPTION, 'dataguard,log,shipping,check,primary' Keywords from dual
union all
select 'dg_psby_diag.sql' SCRIPT,' ' PARAMETERS,'Collect data guard settings from standby' DESCRIPTION, 'dataguard,log,shipping,check,standby' Keywords from dual
union all
select 'grant_awr.sql' SCRIPT,'username ' PARAMETERS,'Generate required privileges to generate AWR reports' DESCRIPTION, 'awr,report,privilege,grant' Keywords from dual
union all
select 'rman_progress.sql' SCRIPT,' ' PARAMETERS,'Show RMAN backup/restore progress' DESCRIPTION, 'rman,restore,backup,progress' Keywords from dual
union all
select 'sqlid_machine.sql' SCRIPT,' ' PARAMETERS,'Show SQL IDs by Machine' DESCRIPTION, 'sqlid,machine' Keywords from dual
union all
select 'log_switch.sql' SCRIPT,' ' PARAMETERS,'Show log switch per day' DESCRIPTION, 'log,switch,day' Keywords from dual
union all
select 'log_switch2.sql' SCRIPT,' ' PARAMETERS,'Show log switch per hour' DESCRIPTION, 'log,switch,hour' Keywords from dual
union all
select 'drv.sql' SCRIPT,' ' PARAMETERS,'Generate required scripts for DRV' DESCRIPTION, 'drv,transition' Keywords from dual
union all
select 'min_datafile.sql' SCRIPT,'datafile, tablespace' PARAMETERS,'Show minimum size of datafile to be resized' DESCRIPTION, 'shrink,hwm,datafile,tablespace' Keywords from dual
union all
select 'session_memory.sql' SCRIPT,' ' PARAMETERS,'Show PGA usage by session' DESCRIPTION, 'session,pga,memory,tuning' Keywords from dual
union all
select 'cache_hit_ratio.sql' SCRIPT,' ' PARAMETERS,'Shows cache hit ration metric' DESCRIPTION, 'memory,cache,hit,ratio,tuning' Keywords from dual
union all
select 'controlfiles.sql' SCRIPT,' ' PARAMETERS,'Shows controlfiles info' DESCRIPTION, 'control,files' Keywords from dual
union all
select 'db_cache_advice.sql' SCRIPT,' ' PARAMETERS,'Predicts changes on buffer cache affects physical reads' DESCRIPTION, 'buffer,cache,physical,read,advice,tunning' Keywords from dual
union all
select 'db_links_open.sql' SCRIPT,' ' PARAMETERS,'Shows all opened db links' DESCRIPTION, 'dblink,open' Keywords from dual
union all
select 'top10sql.sql' SCRIPT,' ' PARAMETERS,'Shows top 10 sqls based on elapsed time' DESCRIPTION, 'top,sql,tuning,elapsed,time' Keywords from dual
union all
select 'sqltune.sql' SCRIPT,'sqlid' PARAMETERS,'Execute SQL Tune for a SQL ID' DESCRIPTION, 'sql,tune,tuning,advisor' Keywords from dual
union all
select 'sql_profile.sql' SCRIPT,' ' PARAMETERS,'Show sql profiles for a SQL ID' DESCRIPTION, 'sql,profile,tuning' Keywords from dual
union all
select 'unused_space.sql' SCRIPT,'owner,name,type,partition' PARAMETERS,'Displays unused space for each segment' DESCRIPTION, 'space,unused,waste,segment,table' Keywords from dual
union all
select 'unused_space2.sql' SCRIPT,'owner,name,tablespace' PARAMETERS,'Displays unused space for list of tables' DESCRIPTION, 'space,unused,waste,table' Keywords from dual
union all
select 'backup_stats.sql' SCRIPT,'owner,backup stats,table name' PARAMETERS,'Generate the code to backup table stats' DESCRIPTION, 'table,backup,statistics,restore,import' Keywords from dual
union all
select 'sql_monitor_report.sql' SCRIPT,'sql id' PARAMETERS,'Show SQL Monitor report of SQL ID' DESCRIPTION, 'sql,monitor,sqlid,sql_id,report' Keywords from dual
union all
select 'awrtopsql.sql' SCRIPT,' ' PARAMETERS,'Show top 5 sql from AWR of last 24h' DESCRIPTION, 'sql,awr,top,tuning' Keywords from dual
union all
select 'awrsql.sql' SCRIPT,'sql id,hash value,schema' PARAMETERS,'Show SQL history based on AWR' DESCRIPTION, 'sql,awr,history,tuning' Keywords from dual
union all
select 'last_exec_sql.sql' SCRIPT,'sql id' PARAMETERS,'Shows last execution of SQL ID' DESCRIPTION, 'sqlid,sql,last,execution' Keywords from dual
union all
select 'check_stats.sql' SCRIPT,'owner,table name,threshold' PARAMETERS,'Shows missing/stale statistics' DESCRIPTION, 'statistics,stale,missin,table,tuning' Keywords from dual
union all
select 'top10_wait.sql' SCRIPT,' ' PARAMETERS,'Shows top 10 wait events' DESCRIPTION, 'top,wait,event,10,tuning' Keywords from dual
union all
select 'sqlhc.sql' SCRIPT,'sql id' PARAMETERS,'Execute SQL Tuning Health Check - Run: sqlhc T <sqlid>' DESCRIPTION, 'sqlhc,tuning' Keywords from dual
union all
select 'compile.sql' SCRIPT,' ' PARAMETERS,'Generate script to recompile invalid objects' DESCRIPTION, 'object,invalid,compile,recompile' Keywords from dual
union all
select 'clear.sql' SCRIPT,' ' PARAMETERS,'Clear all column format settings' DESCRIPTION, 'clear,setting,sqlplus' Keywords from dual
union all
select 'xplan_awr.sql' SCRIPT,'sql id,hash value' PARAMETERS,'Show execution plan from awr' DESCRIPTION, 'xplan,plan,execution,awr' Keywords from dual
union all
select 'xplan_awr2.sql' SCRIPT,'string' PARAMETERS,'Show all execution plans from string' DESCRIPTION, 'xplan,plan,execution,awr,string' Keywords from dual
union all
select 'xplan_baseline.sql' SCRIPT,'sql handle,plan name' PARAMETERS,'Show execution plan from baseline' DESCRIPTION, 'xplan,plan,execution,baseline' Keywords from dual
union all
select 'xplan_baseline2.sql' SCRIPT,'string' PARAMETERS,'Show all execution plans from string' DESCRIPTION, 'xplan,plan,execution,baseline,string' Keywords from dual
union all
select 'xplan_cursor.sql' SCRIPT,'sql id,child cursor no' PARAMETERS,'Show execution plan from cursor cache' DESCRIPTION, 'xplan,plan,execution,cursor,cache' Keywords from dual
union all
select 'xplan_sts.sql' SCRIPT,'name,owner,sql id,hash name' PARAMETERS,'Show execution plan from sts' DESCRIPTION, 'xplan,plan,execution,sts,tuning,set,sql' Keywords from dual
union all
select 'syn.sql' SCRIPT,'synonym name' PARAMETERS,'Shows all related synonyms' DESCRIPTION, 'synonyms' Keywords from dual
union all
select 'blocking.sql' SCRIPT,' ' PARAMETERS,'Shows blocking sessions' DESCRIPTION, 'lock,monitor,session' Keywords from dual
union all
select 'check_ind.sql' SCRIPT,'owner,table name' PARAMETERS,'Shows all indexes from specific table' DESCRIPTION, 'table,indexes,tuning' Keywords from dual
union all
select 'dblinks.sql' SCRIPT,' ' PARAMETERS,'Shows all database links' DESCRIPTION, 'dblink' Keywords from dual
union all
select 'duplicate_user.sql' SCRIPT,'source username, target username, password' PARAMETERS,'Create a new user based on existing one' DESCRIPTION, 'user,duplicate,create' Keywords from dual
union all
select 'ddluser.sql' SCRIPT,'username' PARAMETERS,'Generate user DDL statements' DESCRIPTION, 'user,ddl' Keywords from dual
union all
select 'tab_sql.sql' SCRIPT,'table name' PARAMETERS,'Check the sql_ids executed in a specific table from gv$sql view' DESCRIPTION, 'sql,tuning' Keywords from dual
union all
select 'tab_sql_det.sql' SCRIPT,'table name' PARAMETERS,'Check the sql_ids executed in a specific table from gv$sql view, adding some sql_text info' DESCRIPTION, 'sql,tuning' Keywords from dual
union all
select 'tab_sql_awr.sql' SCRIPT,'table name,awr retention' PARAMETERS,'Check the sql_ids executed in a specific table from AWR snapshots' DESCRIPTION, 'sql,tuning,awr' Keywords from dual
union all
select 'tab_sql_awr_det.sql' SCRIPT,'table name,awr retention' PARAMETERS,'Check the sql_ids executed in a specific table from AWR snapshots detailed' DESCRIPTION, 'sql,tuning,awr' Keywords from dual
union all
select 'sql_obj_awr.sql' SCRIPT,'owner name,table name' PARAMETERS,'Shows all SQL statements based on specific table' DESCRIPTION, 'sql,tuning,awr' Keywords from dual
union all
select 'sql_obj_awr2.sql' SCRIPT,'owner name,table name' PARAMETERS,'Shows query execution times of specific table' DESCRIPTION, 'sql,tuning,awr' Keywords from dual
union all
select 'undo_advisor.sql' SCRIPT,' ' PARAMETERS,'Runs UNDO advisor to gather recommendation about undo usage' DESCRIPTION, 'tuning,undo' Keywords from dual
union all
select 'acls.sql' SCRIPT,' ' PARAMETERS,'Shows current ACLs' DESCRIPTION, 'acl,network' Keywords from dual
union all
select 'tab_siz.sql' SCRIPT,'owner, table name' PARAMETERS,'Shows table size' DESCRIPTION, 'segment,table,size' Keywords from dual
union all
select 'tab_siz_det.sql' SCRIPT,'owner, table name' PARAMETERS,'Shows table size + partitions' DESCRIPTION, 'partition,segment,table,size' Keywords from dual
union all
select 'invalid_obj.sql' SCRIPT,'owner, object name, object type' PARAMETERS,'Shows invalid objects' DESCRIPTION, 'invalid,object' Keywords from dual
union all
select 'find_sqlid.sql' SCRIPT,'sql text' PARAMETERS,'Shows related SQL IDs based on SQL Text' DESCRIPTION, 'sqlid,sqltext' Keywords from dual
union all
select 'sup_log.sql' SCRIPT,'owner, table name' PARAMETERS,'Shows suplemmental log settings for tables' DESCRIPTION, 'supplemental,log,table' Keywords from dual
union all
select 'enable_sup_log.sql' SCRIPT,'owner, table name' PARAMETERS,'Enable supplemental log on tables' DESCRIPTION, 'enable,supplemental,log,table' Keywords from dual
union all
select 'lob_inf.sql' SCRIPT,'owner, table name' PARAMETERS,'Shows lob info of specific table' DESCRIPTION, 'lob,table' Keywords from dual
union all
select 'lob_inf_det.sql' SCRIPT,'owner, table name' PARAMETERS,'Shows detailed lob info of specific table' DESCRIPTION, 'segment,lob,table' Keywords from dual
union all
select 'lob_siz_top.sql' SCRIPT,' ' PARAMETERS,'Shows top 10 biggest lobs' DESCRIPTION, 'segment,top,lob,table' Keywords from dual
union all
select 'tab_siz_top.sql' SCRIPT,' ' PARAMETERS,'Shows top 10 biggest tables' DESCRIPTION, 'segment,top,table' Keywords from dual
union all
select 'st3.sql' SCRIPT,' SQL ID' PARAMETERS,'Shows query execution plan from AWR' DESCRIPTION, 'sqlid,sql,awr' Keywords from dual
union all
select 'st4.sql' SCRIPT,' ' PARAMETERS,'Shows query execution plan from Plan Table (EXPLAIN PLAN FOR)' DESCRIPTION, 'sql,plan table,sqlid,awr' Keywords from dual
union all
select 'check_defrag.sql' SCRIPT,'owner, table name' PARAMETERS,'Shows defragmentation stats' DESCRIPTION, 'defrag,table,index,maintenance,reorg' Keywords from dual
union all
select 'reset_dbsnmp.sql' SCRIPT,'owner, table name' PARAMETERS,'Reset dbsnmp account' DESCRIPTION, 'reset,dbsnmp,password' Keywords from dual
union all
select 'sqltuneawr.sql' SCRIPT,'sql id, begin/end snap' PARAMETERS,'Execute SQL Tune for a SQL ID' DESCRIPTION, 'sql,tune,tuning,advisor,awr' Keywords from dual
union all
select 'check_stalestats.sql' SCRIPT,'owner, table name' PARAMETERS,'Check if an owner/table has stale stats' DESCRIPTION, 'stale,stats,table,owner' Keywords from dual
union all
select 'check_dictstats.sql' SCRIPT,' ' PARAMETERS,'Show dictionary and fixed object stats' DESCRIPTION, 'dict,fixed,stats,stale' Keywords from dual
union all
select 'space_usage.sql' SCRIPT,'owner, table name' PARAMETERS,'Show space stats of given table (DBMS_SPACE)' DESCRIPTION, 'defrag,space,table,segment' Keywords from dual
union all
select 'sqlid_profile.sql' SCRIPT,'sql id' PARAMETERS,'List sql profiles related to a specific sql id' DESCRIPTION, 'sqlid,tuning,awr,profile' Keywords from dual
union all
select 'param_mods.sql' SCRIPT,'parameter name' PARAMETERS,'List database parameter changes based on AWR history' DESCRIPTION, 'awr,parameter,pfile,init,tuning' Keywords from dual
union all
select 'top5sql_addm.sql' SCRIPT,' ' PARAMETERS,'Top 5 queries of past week based on ADDM' DESCRIPTION, 'addm,top,sql,awr,tuning' Keywords from dual
union all
select 'sqlid_hash.sql' SCRIPT,'sql id' PARAMETERS,'Show all hashes of given SQL ID' DESCRIPTION, 'sql,tuning,awr,hash' Keywords from dual
union all
select 'check_sqlid_trends.sql' SCRIPT,'sql id' PARAMETERS,'Shows SQL ID trends of past week' DESCRIPTION, 'sql,tuning,awr,hash,trends' Keywords from dual
union all
select 'topsqlawr.sql' SCRIPT,'date, start/end hour' PARAMETERS,'Shows top queries based on AWR period' DESCRIPTION, 'sql,tuning,awr,period,time,top' Keywords from dual
union all
select 'topsqlcpu.sql' SCRIPT,' ' PARAMETERS,'Shows top queries with high cpu on past day' DESCRIPTION, 'sql,tuning,top,cpu' Keywords from dual
union all
select 'topsqlio.sql' SCRIPT,' ' PARAMETERS,'Shows top queries with high I/O on past day' DESCRIPTION, 'sql,tuning,top,i/o,io' Keywords from dual
union all
select 'topsql1h.sql' SCRIPT,' ' PARAMETERS,'Shows top queries of last 1h' DESCRIPTION, 'sql,tuning,top,1h' Keywords from dual
union all
select 'topsegs.sql' SCRIPT,' ' PARAMETERS,'Shows top aegments by physical reads' DESCRIPTION, 'segments,top,physical,read,tuning' Keywords from dual
union all
select 'topobjwaits.sql' SCRIPT,' ' PARAMETERS,'Shows top objects with waits' DESCRIPTION, 'objects,top,waits,tuning' Keywords from dual
union all
select 'topsqlcpu2.sql' SCRIPT,' ' PARAMETERS,'Shows top queries with high cpu on given period' DESCRIPTION, 'sql,tuning,top,cpu,period' Keywords from dual
union all
select 'dbevents.sql' SCRIPT,'date, start/end hour' PARAMETERS,'Shows database events on given period' DESCRIPTION, 'events,waits,database,awr,tuning' Keywords from dual
union all
select 'topevents_startup.sql' SCRIPT,' ' PARAMETERS,'Shows top events since db startup' DESCRIPTION, 'events,waits,database,awr,tuning,startup' Keywords from dual
union all
select 'topwaitevents.sql' SCRIPT,' ' PARAMETERS,'Shows top wait events' DESCRIPTION, 'events,waits,database,awr,tuning' Keywords from dual
union all
select 'bkp_stats.sql' SCRIPT,'owner, table name' PARAMETERS,'Generate script to backup/restore table stats' DESCRIPTION, 'stats,table,tuning,awr' Keywords from dual
union all
select 'topN.sql' SCRIPT,'schema, qty' PARAMETERS,'Shows Top N queries' DESCRIPTION, 'tuning,sql,awr,query' Keywords from dual
union all
select 'locks.sql' SCRIPT,' ' PARAMETERS,'Shows all current object locks' DESCRIPTION, 'tuning,sql,awr,object,lock,dml,ddl' Keywords from dual
union all
select 'baseline.sql' SCRIPT,'sql handle, plan name' PARAMETERS,'Shows a sql baseline plan' DESCRIPTION, 'tuning,sql,baseline' Keywords from dual
union all
select 'baseline2.sql' SCRIPT,'sqlid' PARAMETERS,'Shows current enabled baseline for a sqlid' DESCRIPTION, 'tuning,sqlid,baseline' Keywords from dual
union all
select 'baseline3.sql' SCRIPT,'sql handle, plan name' PARAMETERS,'Shows all sql baselines' DESCRIPTION, 'tuning,sqlid,baseline' Keywords from dual
union all
select 'sqlbaseline.sql' SCRIPT,'sql handle, plan name, property, value' PARAMETERS,'Change SQL Baseline' DESCRIPTION, 'tuning,sqlid,baseline' Keywords from dual
) where upper(keywords) like upper(decode('&kw',null,keywords,'%&kw%'))
order by 1;

set feedback on