select sum(bytes)/1024/1024/1024 "Datafiles in GB" from v$datafile;
select sum(bytes)/1024/1024/1024 "Tempfiles in GB" from dba_temp_files;
select sum(bytes)/1024/1024/1024 "Undo in GB" from dba_undo_extents;
select sum(bytes)/1024/1024/1024 "Log in GB" from v$log;
select sum(blocks*8)/1024/1024/1024 "Archive in GB" from v$archived_log;