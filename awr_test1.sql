set lines 100 pages 999
select snap_id,
  snap_level,
  to_char(begin_interval_time, 'dd/mm/yy hh24:mi:ss') begin
from 
   dba_hist_snapshot 
order by 1;

set lines 1000
set pages 1000
COLUMN SQL_TEXT                         FORMAT a60
COLUMN username                         FORMAT a15
select
    e.username,
    e.status,
    a.uga_memory,
    b.pga_memory,
    u.SQL_TEXT,
    u.users_executing,    
    TO_CHAR(ROUND(u.RUNTIME_MEM/1024),99999999) || ' KB' RUNTIME_MEM, 
    u.CPU_TIME
from
 (select y.SID, 
  TO_CHAR(ROUND(y.value/1024),99999999) || ' KB' UGA_MEMORY 
  from 
     v$sesstat y, 
     v$statname z 
   where 
     y.STATISTIC# = z.STATISTIC# 
   and 
     NAME = 'session uga memory') a,
 (select 
     y.SID, 
     TO_CHAR(ROUND(y.value/1024),99999999) || ' KB' PGA_MEMORY   
  from 
     v$sesstat y, v$statname z 
  where 
     y.STATISTIC# = z.STATISTIC#   
  and 
     NAME = 'session pga memory') b,
v$session e
inner join 
    v$sqlarea u 
on 
    u.SQL_ID=e.SQL_ID 
where
   e.sid=a.sid 
and 
   e.sid=b.sid
order by
   e.status,
   a.uga_memory desc; 