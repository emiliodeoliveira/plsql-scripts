export BKP_DIR=/tmpnas/goalp_backup
export EMAIL_LIST="fabio_m_fonseca@dell.com Emilio_guilherme_Car@Dellteam.com"
echo " " | mail -s "$ORACLE_SID: Datapump Start - `date`" $EMAIL_LIST
expdp \'/ as sysdba\' directory=exp_goalp dumpfile=exp_goalp_%U.dmp filesize=10G logfile=exp_goalp.log tables=GPW.V_VLEVEL4CATEGORY,GPW.V_VLEVEL3CATEGORY,GPW.PRODUCTCLASS,GPW.PRODUCTMODEL,GPW.V_VPRODUCT,GPW.PRODUCTSKUMAP,GPW.PRODUCTCOST,GPW.PRODUCTGIICLASSMAPPING,GPW.ITEMLOCATION
cat /tmpnas/goalp_backup/exp_goalp.log | mail -s "$ORACLE_SID: Datapump Finish - `date`" $EMAIL_LIST 
