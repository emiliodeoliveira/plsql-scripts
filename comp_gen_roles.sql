set lines 1000 pages 999
COL TYPE FORMAT a12
COL GRANTEE FORMAT a30
COL TABLE_NAME FORMAT a20
COL GRANTED_ROLE FORMAT a20
COL PRIVILEGE a15
COL OWNER FORMAT a15
COL status FORMAT a6

select * from (
	select 
		'ROLE_PRIVS' TYPE,
		db1.GRANTED_ROLE,
		db1.GRANTEE,
		'' TABLE_NAME,
		'' PRIVILEGE,
		'' OWNER,
	case 
		when
			db1.GRANTED_ROLE = db2.GRANTED_ROLE
				then 'TRUE'
		else
			'FALSE'
		end status
	from
		dba_role_privs@ALAEP_LINK db2
	inner join
		dba_role_privs db1
	on
		db1.GRANTEE = db2.GRANTEE
		and db1.GRANTEE like 'ALAE%'
	union all
	select
		'TABLE_PRIVS' TYPE,
		'' GRANTED_ROLE,
		'' GRANTEE,
		db1.TABLE_NAME,
		db1.PRIVILEGE,
		db1.OWNER,
	case 
		when
			db1.PRIVILEGE = db2.PRIVILEGE
				then 'TRUE'
		else
			'FALSE'
		end status
	from
		dba_tab_privs@ALAEP_LINK db2
	inner join
		dba_tab_privs db1
	on
		db1.GRANTEE = db2.GRANTEE
		and db1.GRANTEE like 'ALAE%'
		and db1.TABLE_NAME  =db2.TABLE_NAME
	union all
	select
		'SYS_PRIVS' TYPE,
		'' GRANTED_ROLE,
		db1.GRANTEE,
		'' TABLE_NAME,
		db1.PRIVILEGE,
		'' OWNER,
	case 
		when
			db1.PRIVILEGE = db2.PRIVILEGE
				then 'TRUE'
		else
			'FALSE'
		end status
	from
		dba_sys_privs@ALAEP_LINK db2
	inner join
		dba_sys_privs db1
	on
		db1.GRANTEE = db2.GRANTEE
		and db1.GRANTEE like 'ALAE%'
	)
where
	status = 'FALSE'
order by GRANTED_ROLE;