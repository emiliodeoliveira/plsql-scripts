-- Script made by Emilio de Oliveira
-- E-mail: emilio_guilherme_car@dellteam.com

set lines 1000
set pages 1000
COLUMN SQL_TEXT FORMAT a60
COLUMN operation_type FORMAT a14
select * 
from
 (select 
    w.workarea_address,
    w.operation_type, 
    TO_CHAR(ROUND(w.LAST_MEMORY_USED/1024/1024),99999999) || ' MB' LAST_MEMORY_USED, 
    s.sql_id,
    s.sql_text
from 
    v$sql_workarea w
inner join
    v$sql s on w.sql_id=s.sql_id
order by 
    estimated_optimal_size DESC
    )
where ROWNUM <=10;